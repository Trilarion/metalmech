<?php

define("CONFIGURED", "yes");

//change path to your htdocs dir
define("PATH", "/var/www/demo.metalmech.com/htdocs");
define("DATA", "/var/www/demo.metalmech.com/htdocs");
define("XMLDATA", "/var/www/demo.metalmech.com/xml_data/");

if (FALSE != DEBUG) {
  define("GLOBALSITE", "http://demo.mm"); // without / at the end
} else {
  define("GLOBALSITE", "http://demo.metalmech.com"); // without / at the end
}

$loginurl=GLOBALSITE."/pilot/?action=loginform";
$globalsite=GLOBALSITE;

?>
#!/usr/bin/php
<?PHP
    require_once 'Net/Server.php';
    require_once 'Net/Server/Handler.php';

class Net_Server_Handler_Talkback extends Net_Server_Handler
{
    
    function    onReceiveData( $clientId = 0, $data = "" )
    {
        $data=trim($data);
        $params=explode("|", $data);
        if ($params[0]=="newbattle") {
            unset($bid);
            $bid=$params[1];
            if (!empty($bid)) {
                exec("nohup ./event_newbattle.php ".$bid." 1>event_newbattle.log 2>&1 &");
            }
        }
        $this->_server->sendData( $clientId, "You said: $data\n" );
    }
}
    
    // create a server that forks new processes
    $server  = &Net_Server::create('fork', 'localhost', 9090);

    if (PEAR::isError($server)) {
        echo $server->getMessage()."\n";
    }
    
    $handler = &new Net_Server_Handler_Talkback;
    
    // hand over the object that handles server events
    $server->setCallbackObject($handler);
    
    // start the server
    $server->start();
?>
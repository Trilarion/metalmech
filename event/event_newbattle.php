#!/usr/bin/php
<?php
//event_newbattle.php

define("DEBUG", true);

if (FALSE != DEBUG) {

    require_once 'Benchmark/Timer.php';
 
    $timer = new Benchmark_Timer();
    $timer->start();
}

if (FALSE != DEBUG) {
    define("PATH", "/var/www/demo.metalmech.com/htdocs");
    define("XMLDATA", "/var/www/demo.metalmech.com/htdocs/xml_data/");
} else {
    define("PATH", "/vhosts/boom/mm/demo/public");
    define("XMLDATA", "/vhosts/boom/mm/demo/public/xml_data/");
}


require_once(PATH."/includes/functions.inc");

//timeout of turn
define("TIMEOUT", 120);

//get battle id

$bid=$argv[1];

if (empty($bid)) {
    print "No bid";
    //exception
    exit();
}

define("BID", $bid);
Warn("BID=".BID,__LINE__);

//sleep(30);

?>
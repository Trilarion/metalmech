<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

function drawRating($rating, $max) {
    $inrow=10;
    $h=3;
    $w=3;
    $amax=ceil($max/$inrow);
    $width=ceil($inrow*$w)+1;
    
    $height = ceil($amax*$h)+1;;
//print $width.":".$height;
//   $ratingbar = (($rating/$max)*$width)-1;
   $image = imagecreate($width,$height);
   //colors
   $back = ImageColorAllocate($image,255,255,255);
   $border = ImageColorAllocate($image,153,153,153);
   $red = ImageColorAllocate($image,255,0,0);
   $yellow = ImageColorAllocate($image,255,255,0);
   $green = ImageColorAllocate($image,0,255,0);

   $darkRed = ImageColorAllocate($image,204,0,0);
   $darkYellow = ImageColorAllocate($image,204,204,0);
   $darkGreen = ImageColorAllocate($image,51,153,0);

   $black = ImageColorAllocate($image,0,0,0);
   $pipcolor = ImageColorAllocate($image,83,79,79);
   
   

   ImageFilledRectangle($image,0,0,$width-1,$height-1,$black);
//for by rows


$fullrows=ceil($rating/$inrow);
$fullcels=ceil($rating % $inrow);
$nfullcels=ceil($max % $inrow);

   ImageFilledRectangle($image,0,0,$width,ceil(($amax-1)*$h),$red);
   ImageFilledRectangle($image,0,ceil(($amax-1)*$h),ceil($nfullcels*$w),ceil(($amax)*$h),$red);


ImageFilledRectangle($image,0,0,$width,ceil(($fullrows-1)*$h)-1,$green);

 ImageFilledRectangle($image,0,ceil(($fullrows-1)*$h),ceil(($fullcels)*$w),ceil(($fullrows)*$h),$green);
  



//fill by cols
/*   if(($rating/$max)*100 >= 50)
   {
    ImageFilledRectangle($image,1,1,$ratingbar,10,$darkGreen);
    ImageFilledRectangle($image,1,1,$ratingbar,$height-4,$green);
   }
   elseif (($rating/$max)*100 >= 20)
{
    ImageFilledRectangle($image,1,1,$ratingbar,10,$darkYellow);
    ImageFilledRectangle($image,1,1,$ratingbar,$height-4,$yellow);

} else    {
    ImageFilledRectangle($image,1,1,$ratingbar,10,$darkRed);
    ImageFilledRectangle($image,1,1,$ratingbar,$height-4,$red);
   }
   */
   // pip marks
$pp=$h*$amax;
   for($i = 0; $i <= $inrow; $i++)
   {
    $pip=$i*$w;
    ImageLine($image,$pip,1,$pip,$pp,$pipcolor);

}
$pp=$w*$inrow;
   for($i = 0; $i <= $amax; $i++)
   {
$pip=$i*$h;
    
    ImageLine($image,1,$pip,$pp,$pip,$pipcolor);
   } 
  
   ImageRectangle($image,0,0,$width-1,$height-1,$border);
   imagePNG($image);
  imagedestroy($image);
   
   
}

Header("Content-type: image/png");
$rating=$_GET["rating"];
if (!isset($rating)) {
    $rating=0;
}

$max=$_GET["max"];
if (!isset($max)) {
    $max=10;
}

if ($max<$rating) {
    $max=$rating;
}

drawRating($rating, $max);
?>
{config_load file=test.conf section="setup"}
{assign var="onload" value="show('m');"}
{include file="header.tpl" title=foo}


<br/>
Pilot: <b>{$mech_pilot_name}</b><br/><br/>
Heat: <b>[{$mech_heat}/{$mech_maxheat}]</b><br/>
Speed: <b>{$mech_speed}</b><br/>
Weight: <b>{$mech_weight}</b><br/>
Load weight: <b>[{$mech_loadweight}/{$mech_maxloadweight}]</b><br/>
Status: <b>{$mech_status}</b><br/>

{include file="mechinfo.tpl"}

{include file="footer.tpl"}
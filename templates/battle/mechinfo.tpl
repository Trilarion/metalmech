
<br/>
{literal}
<script language="JavaScript">
<!--
function show(val) {
    var maxi=7;

    for (i=0;i<=maxi;i++) {
        eval("m"+i).style.display='none';
        eval("m"+i).style.visibility='hidden';

        eval("w"+i).style.display='none';
        eval("w"+i).style.visibility='hidden';
        eval("a"+i).style.display='none';
        eval("a"+i).style.visibility='hidden';
        eval("d"+i).style.display='none';
        eval("d"+i).style.visibility='hidden';
    
        eval(val+i).style.display='';
        eval(val+i).style.visibility='visible';
    }

}
//-->
</script>
{/literal}
<table border="1" width="100%" cellspacing="0" bgcolor="#ffffff">
<tr><td  colspan="3" align="center" valign="top">
<table bgcolor="#eeeeee" width="100%">
<tr>

<td align="center" valign="top"><font size="-2"><b>
<input type="button" name="h_show" value="M" onclick="show('m');">
</b></font></td>
<td align="center" valign="top"><font size="-2"><b>
<input type="button" name="h_show" value="W" onclick="show('w');">
</b></font></td>
<td align="center" valign="top"><font size="-2"><b>
<input type="button" name="h_show" value="A" onclick="show('a');">
</b></font></td>
<td align="center" valign="top"><font size="-2"><b>
<input type="button" name="h_show" value="D" onclick="show('d');">
</b></font></td>
<td>
<table border="0" width="100%">
<tr name="m0" id="m0" style="DISPLAY: none; VISIBILITY: hidden">
<td><b>Mech</b></td>
</tr>
<tr name="w0" id="w0" style="DISPLAY: none; VISIBILITY: hidden">
<td><b>Weapon</b></td>
</tr>
<tr name="a0" id="a0" style="DISPLAY: none; VISIBILITY: hidden">
<td><b>Armor</b></td>
</tr>
<tr name="d0" id="d0" style="DISPLAY: none; VISIBILITY: hidden">
<td><b>Devices</b></td>
</tr>

</table>
</td>
</tr>
</table>
</td>
</tr>
    <tr>
      <td colspan="3" align="center" valign="top">
Head
<table border="0" bgcolor="#eeeeee">
<tr  name="m1" id="m1" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.h}&max={$hpmax.h}" alt="{$hp.h}/{$hpmax.h}"/><br/>[{$hp.h}/{$hpmax.h}]</td></tr>
<tr name="w1" id="w1" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0">
{foreach name=outer item=w_h from=$weapon_h}
{if $w_h.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_h.name}<br/>
<img src="/statusbar.php?rating={$w_h.hp}&max={$w_h.hpmax}" alt="{$w_h.hp}/{$w_h.hpmax}"/>[{$w_h.hp}/{$w_h.hpmax}]&nbsp;{if $w_h.distance != ''}
<b>({if $w_h.distance eq 'unlimited'}u{else}{$w_h.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_h.type eq 'rocket' || $w_h.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_h.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table></td>
</tr>
<tr name="a1" id="a1" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_h from=$armor_h}
{if $a_h.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_h.name}<br/>
<img src="/statusbar.php?rating={$a_h.hp}&max={$a_h.hpmax}" alt="{$a_h.hp}/{$a_h.hpmax}"/><br/>[{$a_h.hp}/{$a_h.hpmax}]<b>[{$a_h.dmg}]</b>
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>

</tr>
<tr name="d1" id="d1" style="DISPLAY: none; VISIBILITY: hidden">
<td>
<table border="0" cellspacing="0">
{foreach name=outer item=d_h from=$device_h}
{if $d_h.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_h.name}<br/>
<img src="/statusbar.php?rating={$d_h.hp}&max={$d_h.hpmax}" alt="{$d_h.hp}/{$d_h.hpmax}"/><br/>[{$d_h.hp}/{$d_h.hpmax}]
{if $d_h.heating!=''}
<b>[{$d_h.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table>
</td>
</tr>
</table>



</td>
    </tr>
    <tr>
      <td width="30%" align="center" valign="top">
Left Hand
<table border="0" bgcolor="#eeeeee">
<tr name="m2" id="m2" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.lh}&max={$hpmax.lh}" alt="{$hp.lh}/{$hpmax.lh}"/><br/>[{$hp.lh}/{$hpmax.lh}]</td></tr>
<tr name="w2" id="w2" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0">
{foreach name=outer item=w_lh from=$weapon_lh}
{if $w_lh.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_lh.name}<br/>
<img src="/statusbar.php?rating={$w_lh.hp}&max={$w_lh.hpmax}" alt="{$w_lh.hp}/{$w_lh.hpmax}"/>[{$w_lh.hp}/{$w_lh.hpmax}]&nbsp;{if $w_lh.distance != ''}
<b>({if $w_lh.distance eq 'unlimited'}u{else}{$w_lh.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_lh.type eq 'rocket' || $w_lh.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_lh.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table>
</td>
</tr>
<tr name="a2" id="a2" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_lh from=$armor_lh}
{if $a_lh.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_lh.name}<br/>
<img src="/statusbar.php?rating={$a_lh.hp}&max={$a_lh.hpmax}" alt="{$a_lh.hp}/{$a_lh.hpmax}"/><br/>[{$a_lh.hp}/{$a_lh.hpmax}]<b>[{$a_lh.dmg}]</b>
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
<tr name="d2" id="d2" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=d_lh from=$device_lh}
{if $d_lh.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_lh.name}<br/>
<img src="/statusbar.php?rating={$d_lh.hp}&max={$d_lh.hpmax}" alt="{$d_lh.hp}/{$d_lh.hpmax}"/><br/>[{$d_lh.hp}/{$d_lh.hpmax}]
{if $d_lh.heating!=''}
<b>[{$d_lh.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table>
</td>
</tr>
</table>

</td>
      <td  width="40%" align="center" valign="top">Front
<table  border="0" bgcolor="#eeeeee">
<tr name="m3" id="m3" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.f}&max={$hpmax.f}" alt="{$hp.f}/{$hpmax.f}"/><br/>[{$hp.f}/{$hpmax.f}]</td></tr>
<tr name="w3" id="w3" style="DISPLAY: none; VISIBILITY: hidden">
<td>
<table border="0">
{foreach name=outer item=w_f from=$weapon_f}
{if $w_f.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_f.name}<br/>
<img src="/statusbar.php?rating={$w_f.hp}&max={$w_f.hpmax}" alt="{$w_f.hp}/{$w_f.hpmax}"/>[{$w_f.hp}/{$w_f.hpmax}]&nbsp;{if $w_f.distance != ''}
<b>({if $w_f.distance eq 'unlimited'}u{else}{$w_f.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_f.type eq 'rocket' || $w_f.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_f.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table>
</td>
</tr>
<tr name="a3" id="a3" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_f from=$armor_f}
{if $a_f.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_f.name}</b><br/>
<img src="/statusbar.php?rating={$a_f.hp}&max={$a_f.hpmax}" alt="{$a_f.hp}/{$a_f.hpmax}"/><br/>[{$a_f.hp}/{$a_f.hpmax}]<b>[{$a_f.dmg}]
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
<tr name="d3" id="d3" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=d_f from=$device_f}
{if $d_f.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_f.name}<br/>
<img src="/statusbar.php?rating={$d_f.hp}&max={$d_f.hpmax}" alt="{$d_f.hp}/{$d_f.hpmax}"/><br/>[{$d_f.hp}/{$d_f.hpmax}]
{if $d_f.heating!=''}
<b>[{$d_f.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table>
</td>
</tr>
</table>

</td>
      <td width="30%" align="center" valign="top">Right Hand
<table  border="0" bgcolor="#eeeeee">
<tr name="m4" id="m4" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.rh}&max={$hpmax.rh}" alt="{$hp.rh}/{$hpmax.rh}"/><br/>[{$hp.rh}/{$hpmax.rh}]</td></tr>
<tr name="w4" id="w4" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0">
{foreach name=outer item=w_rh from=$weapon_rh}
{if $w_rh.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_rh.name}<br/>
<img src="/statusbar.php?rating={$w_rh.hp}&max={$w_rh.hpmax}" alt="{$w_rh.hp}/{$w_rh.hpmax}"/>[{$w_rh.hp}/{$w_rh.hpmax}]&nbsp;{if $w_rh.distance != ''}
<b>({if $w_rh.distance eq 'unlimited'}u{else}{$w_rh.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_rh.type eq 'rocket' || $w_rh.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_rh.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table></td>
</tr>
<tr name="a4" id="a4" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_rh from=$armor_rh}
{if $a_rh.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_rh.name}<br/>
<img src="/statusbar.php?rating={$a_rh.hp}&max={$a_rh.hpmax}" alt="{$a_rh.hp}/{$a_rh.hpmax}"/><br/>[{$a_rh.hp}/{$a_rh.hpmax}]<b>[{$a_rh.dmg}]</b>
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
<tr name="d4" id="d4" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=d_rh from=$device_rh}
{if $d_rh.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_rh.name}<br/>
<img src="/statusbar.php?rating={$d_rh.hp}&max={$d_rh.hpmax}" alt="{$d_rh.hp}/{$d_rh.hpmax}"/><br/>[{$d_rh.hp}/{$d_rh.hpmax}]
{if $d_rh.heating!=''}
<b>[{$d_rh.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
</table>

</td>

    </tr>
    <tr>
      <td align="center" valign="top">Left Leg
<table  border="0" bgcolor="#eeeeee">
<tr name="m5" id="m5" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.ll}&max={$hpmax.ll}" alt="{$hp.ll}/{$hpmax.ll}"/><br/>[{$hp.ll}/{$hpmax.ll}]</td></tr>
<tr name="w5" id="w5" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0">
{foreach name=outer item=w_ll from=$weapon_ll}
{if $w_ll.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_ll.name}<br/>
<img src="/statusbar.php?rating={$w_ll.hp}&max={$w_ll.hpmax}" alt="{$w_ll.hp}/{$w_ll.hpmax}"/>[{$w_ll.hp}/{$w_ll.hpmax}]&nbsp;{if $w_ll.distance != ''}
<b>({if $w_ll.distance eq 'unlimited'}u{else}{$w_ll.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_ll.type eq 'rocket' || $w_ll.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_ll.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table></td>
</tr>
<tr name="a5" id="a5" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_ll from=$armor_ll}
{if $a_ll.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_ll.name}<br/>
<img src="/statusbar.php?rating={$a_ll.hp}&max={$a_ll.hpmax}" alt="{$a_ll.hp}/{$a_ll.hpmax}"/><br/>[{$a_ll.hp}/{$a_ll.hpmax}]<b>[{$a_ll.dmg}]</b>
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
<tr name="d5" id="d5" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=d_ll from=$device_ll}
{if $d_ll.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_ll.name}<br/>
<img src="/statusbar.php?rating={$d_ll.hp}&max={$d_ll.hpmax}" alt="{$d_ll.hp}/{$d_ll.hpmax}"/><br/>[{$d_ll.hp}/{$d_ll.hpmax}]
{if $d_ll.heating!=''}
<b>[{$d_ll.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
</table>

</td>
      <td align="center" valign="top">Rear
<table  border="0" bgcolor="#eeeeee">
<tr name="m6" id="m6" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.r}&max={$hpmax.r}" alt="{$hp.r}/{$hpmax.r}"/><br/>[{$hp.r}/{$hpmax.r}]</td></tr>
<tr name="w6" id="w6" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0">
{foreach name=outer item=w_r from=$weapon_r}
{if $w_r.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_r.name}<br/>
<img src="/statusbar.php?rating={$w_r.hp}&max={$w_r.hpmax}" alt="{$w_r.hp}/{$w_r.hpmax}"/>[{$w_r.hp}/{$w_r.hpmax}]&nbsp;{if $w_r.distance != ''}
<b>({if $w_r.distance eq 'unlimited'}u{else}{$w_r.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_r.type eq 'rocket' || $w_r.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_r.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table></td>
</tr>
<tr name="a6" id="a6" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_r from=$armor_r}
{if $a_r.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_r.name}<br/>
<img src="/statusbar.php?rating={$a_r.hp}&max={$a_r.hpmax}" alt="{$a_r.hp}/{$a_r.hpmax}"/><br/>[{$a_r.hp}/{$a_r.hpmax}]<b>[{$a_r.dmg}]</b>
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
<tr name="d6" id="d6" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=d_r from=$device_r}
{if $d_f.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_r.name}<br/>
<img src="/statusbar.php?rating={$d_r.hp}&max={$d_r.hpmax}" alt="{$d_r.hp}/{$d_r.hpmax}"/><br/>[{$d_r.hp}/{$d_r.hpmax}]
{if $d_r.heating!=''}
<b>[{$d_r.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
</table>

</td>
      <td align="center" valign="top">Right Leg
<table  border="0" bgcolor="#eeeeee">
<tr name="m7" id="m7" style="DISPLAY: none; VISIBILITY: hidden"><td style="font-size: 10px;" align="center"><img src="/statusbar.php?rating={$hp.rl}&max={$hpmax.rl}" alt="{$hp.rl}/{$hpmax.rl}"/><br/>[{$hp.rl}/{$hpmax.rl}]</td></tr>
<tr name="w7" id="w7" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0">
{foreach name=outer item=w_rl from=$weapon_rl}
{if $w_rl.hp>0}
<tr><td><td style="font-size: 10px;" align="center">{$w_rl.name}<br/>
<img src="/statusbar.php?rating={$w_rl.hp}&max={$w_rl.hpmax}" alt="{$w_rl.hp}/{$w_rl.hpmax}"/>[{$w_rl.hp}/{$w_rl.hpmax}]&nbsp;{if $w_rl.distance != ''}
<b>({if $w_rl.distance eq 'unlimited'}u{else}{$w_rl.distance}{/if})</b>
{/if}
{if $isenemy == 0}
{if $w_rl.type eq 'rocket' || $w_rl.type eq 'gun'}
{foreach name=pack item=w_pack from=$w_rl.pack}
<table border="0" class="ammo" align="center">
<tr><td class="ammo">{$w_pack.name}<br/>
<img src="/amountbar.php?rating={$w_pack.amount}&max={$w_pack.amountmax}" alt="{$w_pack.amount}/{$w_pack.amountmax}"/>[{$w_pack.amount}/{$w_pack.amountmax}|{$w_pack.loaduse}]&nbsp;<b>({$w_pack.distance})&nbsp;[{$w_pack.dmg}]</b></td></tr>
</table>
{/foreach}
{/if}
{/if}
</td></tr>
{/if}
{/foreach}

</table></td>
</tr>
<tr name="a7" id="a7" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=a_rl from=$armor_rl}
{if $a_rl.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$a_rl.name}<br/>
<img src="/statusbar.php?rating={$a_rl.hp}&max={$a_rl.hpmax}" alt="{$a_rl.hp}/{$a_rl.hpmax}"/><br/>[{$a_rl.hp}/{$a_rl.hpmax}]<b>[{$a_rl.dmg}]</b>
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
<tr name="d7" id="d7" style="DISPLAY: none; VISIBILITY: hidden">
<td><table border="0" cellspacing="0">
{foreach name=outer item=d_rl from=$device_rl}
{if $d_rl.hp>0}
<tr>
<td style="font-size: 10px;" align="left" valign="top" nowrap>
{$d_rl.name}<br/>
<img src="/statusbar.php?rating={$d_rl.hp}&max={$d_rl.hpmax}" alt="{$d_rl.hp}/{$d_rl.hpmax}"/><br/>[{$d_rl.hp}/{$d_rl.hpmax}]
{if $d_rl.heating!=''}
<b>[{$d_rl.heating}]</b>
{/if}
</td>
</tr>
{/if}
{/foreach}
<tr><td></td></tr>
</table></td>
</tr>
</table>

</td>

    </tr>
</table>

<br/>


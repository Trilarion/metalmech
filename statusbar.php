<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

function drawRating($rating, $max) {

    //$width=ceil($max*5/10)+1;
    $width=41;
//   $width = isset($_GET['width']) ? $_GET['width'] : 51;
   $height = 6;
   $ratingbar = (($rating/$max)*$width)-1;
   $image = imagecreate($width,$height);
   //colors
   $back = ImageColorAllocate($image,255,255,255);
   $border = ImageColorAllocate($image,153,153,153);
   $red = ImageColorAllocate($image,255,0,0);
   $yellow = ImageColorAllocate($image,255,255,0);
   $green = ImageColorAllocate($image,0,255,0);

   $darkRed = ImageColorAllocate($image,204,0,0);
   $darkYellow = ImageColorAllocate($image,204,204,0);
   $darkGreen = ImageColorAllocate($image,102,204,0);

   $black = ImageColorAllocate($image,0,0,0);
   $pipcolor = ImageColorAllocate($image,83,79,79);
   
   


   ImageFilledRectangle($image,0,0,$width-1,$height-1,$back);
   if(($rating/$max)*100 >= 50)
   {
    ImageFilledRectangle($image,0,1,$ratingbar,10,$green);
    //ImageFilledRectangle($image,0,1,$ratingbar,$height-4,$green);
   }
   elseif (($rating/$max)*100 >= 30)
{
    ImageFilledRectangle($image,0,1,$ratingbar,10,$yellow);
    //ImageFilledRectangle($image,0,1,$ratingbar,$height-4,$yellow);

} else    {
    ImageFilledRectangle($image,0,1,$ratingbar,10,$red);
    //ImageFilledRectangle($image,0,1,$ratingbar,$height-4,$red);
   }
   
   // pip marks
/*
   for($i = 0; $i <= 11; $i++)
   {
    $pip = $i * 5;
    ImageLine($image,$pip,1,$pip,5,$pipcolor);
   } 
   */
   ImageRectangle($image,0,0,$width-1,$height-1,$border);
   imagePNG($image);
   imagedestroy($image);
   
   
}

Header("Content-type: image/png");
$rating=$_GET["rating"];
if (!isset($rating)) {
    $rating=0;
}

$max=$_GET["max"];
if (!isset($max) || $max<=0) {
    $max=100;
}

if ($max<$rating) {
    $max=$rating;
}
drawRating($rating, $max);
?>
<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

//plant.php 


define("DEBUG", false);

if (FALSE != DEBUG) {

    require_once 'Benchmark/Timer.php';
 
    $timer = new Benchmark_Timer();
    $timer->start();
}

require 'Smarty/Smarty.class.php';

$smarty = new Smarty;

$smarty->compile_check = true;

if (FALSE != DEBUG) {
    $smarty->debugging = false;
} else {
    $smarty->debugging = false;
}

    $smarty->template_dir = '/var/www/demo.metalmech.com/htdocs/templates/plant/';
    $smarty->compile_dir = '/var/www/demo.metalmech.com/htdocs/templates_c/plant/';
    $smarty->config_dir = '/var/www/demo.metalmech.com/htdocs/configs/';

//change path to your htdocs dir
    define("PATH", "/var/www/demo.metalmech.com/htdocs");
    define("XMLDATA", "/var/www/demo.metalmech.com/htdocs/xml_data/");

if (FALSE != DEBUG) {
    $site="http://demo.mm/plant/";
    $loginurl="http://demo.mm/pilot/?action=loginform";
} else {
    $site="http://demo.metalmech.com/plant/";
    $loginurl="http://demo.metalmech.com/pilot/?action=loginform";
}

$smarty->assign("site", $site);

require_once(PATH."/includes/functions.inc");

session_start();

//get battle id
$sid=session_id();

if (empty($sid)) {
    print "No sid";
    //exception
    exit();
}

define("SID", $sid);
Warn("SID=".SID,__LINE__);
//get action
    
if (chk_login()) {

    $action=$_GET["action"];
    if (empty($action)) {
        $action=$_POST["action"];
    }
        
    switch($action) {
    
        case "uinfo":
        //mech info in left frame

;
            $battle = new DOMDocument();
            $ret = @$battle->load(XMLDATA."pilot/pilot_".$_SESSION["login"].".xml");
            if (!$ret) {
                echo "Error while parsing the document\n".__LINE__;
                exit;
            }

            $xpath = new domXPath($battle);
            

            $mech = $xpath->query("/pilot/world/mechs/mech");
//print $mech->length;
                
            
//------------
//rewrite for xpath
// // mech[pilot/@name='You']
            foreach ($mech as $mech_tags) {
                $param = $mech_tags->getElementsByTagName("params");
                $param_tags = $param->item(0);
            //rewrite to function
            //init params
                $Xmech_heat=0;
                $Xmech_maxheat=0;
                $Xmech_speed=0;
                $Xmech_weight=0;
                $Xmech_loadweight=0;
                $Xmech_maxloadweight=0;

                $param_status = $mech_tags->getElementsByTagName("status");

                if ($param_status->length > 0) {
                    $Xmech_status=$param_status->item(0)->getAttribute("val");
                } else {
                    $Xmech_status="live";
                }

                $smarty->assign("mech_status",$Xmech_status);

                $param_heat = $param_tags->getElementsByTagName("heat");
                $Xmech_heat=$param_heat->item(0)->getAttribute("val");
                $smarty->assign("mech_heat",$Xmech_heat);

                $param_maxheat = $param_tags->getElementsByTagName("maxheat");
                $Xmech_maxheat=$param_maxheat->item(0)->getAttribute("val");
                $smarty->assign("mech_maxheat",$Xmech_maxheat);

                $param_speed = $param_tags->getElementsByTagName("speed");
                $Xmech_speed=$param_speed->item(0)->getAttribute("val");
                $smarty->assign("mech_speed",$Xmech_speed);

                $param_weight = $param_tags->getElementsByTagName("weight");
                if ($param_weight->length>0) {
                    $Xmech_weight=$param_weight->item(0)->getAttribute("val");
                    $smarty->assign("mech_weight",$Xmech_weight);
                }

                $param_loadweight = $param_tags->getElementsByTagName("loadweight");
                if ($param_loadweight->length>0) {
                    $Xmech_loadweight=$param_loadweight->item(0)->getAttribute("val");
                    $smarty->assign("mech_loadweight",$Xmech_loadweight);
                }

                //$param_maxloadweight = $param_tags->getElementsByTagName("maxloadweight");
                //if (count($param_maxloadweight)>0) {
                // $Xmech_maxloadweight=$param_maxloadweight->item(0)->getAttribute("val");
                $Xmech_maxloadweight=floor(0.5*$Xmech_weight);
                $smarty->assign("mech_maxloadweight",$Xmech_maxloadweight);
                //}


                //get body info
                unset($hp);
                $hp=array();
                $hp["h"]=0;
                $hp["lh"]=0;
                $hp["rh"]=0;
                $hp["ll"]=0;
                $hp["rl"]=0;
                $hp["f"]=0;
                $hp["r"]=0;



                $body = $mech_tags->getElementsByTagName("body");
                $body_tags = $body->item(0);

                //optimize
                $body_h = $body_tags->getElementsByTagName("h");
                $body_h_tags = $body_h->item(0);

                $h_hp=$body_h_tags->getElementsByTagName("hp");
                if ($h_hp->length>0) {
                    $hp["h"]=$h_hp->item(0)->getAttribute("val");
                }


                $body_lh = $body_tags->getElementsByTagName("lh");
                $body_lh_tags = $body_lh->item(0);

                $lh_hp=$body_lh_tags->getElementsByTagName("hp");
                if ($lh_hp->length>0) {
                    $hp["lh"]=$lh_hp->item(0)->getAttribute("val");
                }


                $body_rh = $body_tags->getElementsByTagName("rh");
                $body_rh_tags = $body_rh->item(0);

                $rh_hp=$body_rh_tags->getElementsByTagName("hp");
                if ($rh_hp->length>0) {
                    $hp["rh"]=$rh_hp->item(0)->getAttribute("val");
                }


                $body_ll = $body_tags->getElementsByTagName("ll");
                $body_ll_tags = $body_ll->item(0);

                $ll_hp=$body_ll_tags->getElementsByTagName("hp");
                if ($ll_hp->length>0) {
                    $hp["ll"]=$ll_hp->item(0)->getAttribute("val");
                }

                $body_rl = $body_tags->getElementsByTagName("rl");
                $body_rl_tags = $body_rl->item(0);

                $rl_hp=$body_rl_tags->getElementsByTagName("hp");
                if ($rl_hp->length>0) {
                    $hp["rl"]=$rl_hp->item(0)->getAttribute("val");
                }


                $body_f = $body_tags->getElementsByTagName("f");
                $body_f_tags = $body_f->item(0);

                $f_hp=$body_f_tags->getElementsByTagName("hp");
                if ($f_hp->length>0) {
                    $hp["f"]=$f_hp->item(0)->getAttribute("val");
                }


                $body_r = $body_tags->getElementsByTagName("r");
                $body_r_tags = $body_r->item(0);

                $r_hp=$body_r_tags->getElementsByTagName("hp");
                if ($r_hp->length>0) {
                    $hp["r"]=$r_hp->item(0)->getAttribute("val");
                }

            //end of optimize
                //------------

                $pppoint=array("lh","rh","ll","rl","h","f", "r");

                while(list($k, $v)=each($pppoint)) {

                    unset($aid);
                    unset(${"a_".$v});
                    ${"a_".$v}=array();

                    $ar=${"body_".$v."_tags"}->getElementsByTagName("armor");

                    if ($ar->length>0) {

                        foreach ($ar as $ar_tags) {
                            unset($ainfo);
                            $ainfo=array();
                            $aid=$ar_tags->getAttribute("aid");

                            unset($tmp);
                            $tmp=$ar_tags->getElementsByTagName("name");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $ainfo["name"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$ar_tags->getElementsByTagName("hp");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $ainfo["hp"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$ar_tags->getElementsByTagName("lvl");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $ainfo["lvl"]=$tmp2;
                            }
                            unset($tmp);
                            $tmp=$ar_tags->getElementsByTagName("weight");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $ainfo["weight"]=$tmp2;
                            }
                            unset($tmp);
                            $tmp=$ar_tags->getElementsByTagName("dmgresist");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $ainfo["dmg"]=$tmp2;
                            }


                            ${"a_".$v}[$aid]=$ainfo;
                        }
                    }

                    $wp=${"body_".$v."_tags"}->getElementsByTagName("weapon");

                    if ($wp->length>0) {

                        foreach ($wp as $wp_tags) {
                            unset($winfo);
                            $winfo=array();
                            $wid=$wp_tags->getAttribute("wid");

                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("name");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $winfo["name"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("hp");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $winfo["hp"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("lvl");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $winfo["lvl"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("weight");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $winfo["weight"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("wtype");

                            unset($wtype);

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $winfo["type"]=$tmp2;
                                $wtype=$tmp2;
                            }

                            if ($wtype == "pulse") {
                                unset($tmp);
                                $tmp=$wp_tags->getElementsByTagName("distance");

                                if ($tmp->length>0) {
                                    $tmp2=$tmp->item(0)->getAttribute("val");

                                    $winfo["distance"]=$tmp2;
                                }

                            }

                            if (!$isenemy) {
                                if (($wtype == "rocket")  || ($wtype == "gun")){
                                    $wpack=$wp_tags->getElementsByTagName("load");

                                    if ($wpack->length>0) {


                                        foreach ($wpack as $wpack_tags) {
                                            unset($wpinfo);
                                            unset($lid);
                                            $lid=$wpack_tags->getAttribute("lid");

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("name");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["name"]=$tmp2;
                                            }

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("amount");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["amount"]=$tmp2;
                                            }

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("lvl");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["lvl"]=$tmp2;
                                            }

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("weapdamage");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["dmg"]=$tmp2;
                                            }

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("loaduse");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["loaduse"]=$tmp2;
                                            }

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("distance");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["distance"]=$tmp2;
                                            }

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("weight");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                $wpinfo["weight"]=$tmp2;
                                            }

                                        $winfo["pack"][$lid]=$wpinfo;
                                        }
                                    }
                                }
                            }

                            ${"w_".$v}[$wid]=$winfo;
                        }
                    }
                //----------
                }
            }
            
//-----------

            reset($mech);
            foreach ($mech as $mech_tags) {

                unset($hpmax);
                $hpmax=array();
                $hpmax["h"]=0;
                $hpmax["lh"]=0;
                $hpmax["rh"]=0;
                $hpmax["ll"]=0;
                $hpmax["rl"]=0;
                $hpmax["f"]=0;
                $hpmax["r"]=0;

                $body = $mech_tags->getElementsByTagName("body");
                $body_tags = $body->item(0);

                $body_h = $body_tags->getElementsByTagName("h");
                $body_h_tags = $body_h->item(0);

                $h_hp=$body_h_tags->getElementsByTagName("hp");
                if ($h_hp->length>0) {
                    $hpmax["h"]=$h_hp->item(0)->getAttribute("val");
                }


                $body_lh = $body_tags->getElementsByTagName("lh");
                $body_lh_tags = $body_lh->item(0);

                $lh_hp=$body_lh_tags->getElementsByTagName("hp");
                if ($lh_hp->length>0) {
                    $hpmax["lh"]=$lh_hp->item(0)->getAttribute("val");
                }


                $body_rh = $body_tags->getElementsByTagName("rh");
                $body_rh_tags = $body_rh->item(0);

                $rh_hp=$body_rh_tags->getElementsByTagName("hp");
                if ($rh_hp->length>0) {
                    $hpmax["rh"]=$rh_hp->item(0)->getAttribute("val");
                }


                $body_ll = $body_tags->getElementsByTagName("ll");
                $body_ll_tags = $body_ll->item(0);

                $ll_hp=$body_ll_tags->getElementsByTagName("hp");
                if ($ll_hp->length>0) {
                    $hpmax["ll"]=$ll_hp->item(0)->getAttribute("val");
                }

                $body_rl = $body_tags->getElementsByTagName("rl");
                $body_rl_tags = $body_rl->item(0);

                $rl_hp=$body_rl_tags->getElementsByTagName("hp");
                if ($rl_hp->length>0) {
                    $hpmax["rl"]=$rl_hp->item(0)->getAttribute("val");
                }


                $body_f = $body_tags->getElementsByTagName("f");
                $body_f_tags = $body_f->item(0);

                $f_hp=$body_f_tags->getElementsByTagName("hp");
                if ($f_hp->length>0) {
                    $hpmax["f"]=$f_hp->item(0)->getAttribute("val");
                }


                $body_r = $body_tags->getElementsByTagName("r");
                $body_r_tags = $body_r->item(0);

                $r_hp=$body_r_tags->getElementsByTagName("hp");
                if ($r_hp->length>0) {
                    $hpmax["r"]=$r_hp->item(0)->getAttribute("val");
                }


            //------------

                $pppoint=array("lh","rh","ll","rl","h","f", "r");

                while(list($k, $v)=each($pppoint)) {

                    unset($aid);

                    $ar=${"body_".$v."_tags"}->getElementsByTagName("armor");

                    if ($ar->length>0) {
                        foreach ($ar as $ar_tags) {
                            $aid=$ar_tags->getAttribute("aid");


                            unset($tmp);
                            $tmp=$ar_tags->getElementsByTagName("hp");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                ${"a_".$v}[$aid]["hpmax"]=$tmp2;
                            }
                        }
                    }

                    $wp=${"body_".$v."_tags"}->getElementsByTagName("weapon");

                    if ($wp->length>0) {
                        foreach ($wp as $wp_tags) {
                            $wid=$wp_tags->getAttribute("wid");


                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("hp");

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                ${"w_".$v}[$wid]["hpmax"]=$tmp2;
                            }

                            unset($tmp);
                            $tmp=$wp_tags->getElementsByTagName("wtype");

                            unset($wtype);

                            if ($tmp->length>0) {
                                $tmp2=$tmp->item(0)->getAttribute("val");
                                $wtype=$tmp2;
                            }

                            if (!$isenemy) {
                                if (($wtype == "rocket") || ($wtype == "gun")){
                                    $wpack=$wp_tags->getElementsByTagName("load");

                                    if ($wpack->length>0) {


                                        foreach ($wpack as $wpack_tags) {
                                            unset($wpinfo);
                                            unset($lid);
                                            $lid=$wpack_tags->getAttribute("lid");

                                            unset($tmp);
                                            $tmp=$wpack_tags->getElementsByTagName("amount");

                                            if ($tmp->length>0) {
                                                $tmp2=$tmp->item(0)->getAttribute("val");
                                                //$wpinfo["name"]=$tmp2;
                                                ${"w_".$v}[$wid]["pack"][$lid]["amountmax"]=$tmp2;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                }
                //----------
            }
                
                
            $smarty->assign("hp",$hp);
            $smarty->assign("hpmax",$hpmax);

            //get armor info

            $smarty->assign("armor_h",$a_h);
            $smarty->assign("armor_lh",$a_lh);
            $smarty->assign("armor_rh",$a_rh);
            $smarty->assign("armor_f",$a_f);
            $smarty->assign("armor_r",$a_r);
            $smarty->assign("armor_ll",$a_ll);
            $smarty->assign("armor_rl",$a_rl);

            $smarty->assign("weapon_h",$w_h);
            $smarty->assign("weapon_lh",$w_lh);
            $smarty->assign("weapon_rh",$w_rh);
            $smarty->assign("weapon_f",$w_f);
            $smarty->assign("weapon_r",$w_r);
            $smarty->assign("weapon_ll",$w_ll);
            $smarty->assign("weapon_rl",$w_rl);


            //get weapon info

            //get mech status
            $smarty->assign("mech_pilot_name","pilot");
            $smarty->assign("isenemy",0);



            $smarty->display('uinfo.tpl');
                
            
        

    

        break;
        
        case "repair":
        //repair thing
        break;
        
        case "buy":
        //buy 
        break;
        
        case "sell":
        //sell
        break;
        
        case "move":
        //move thing over mech
        break;
        
        case "emove":
        //move thing over others thing in mech
        break;
        
        case "add":
        //add thing to mech
        break;
        
        case "remove":
        //remove thing from mech
        break;
        
        case "shop":
        //write shop template
           
            $smarty->display('shop.tpl');

        break;
        
        default:
            $smarty->display('index.tpl');
        break;
        
        
    }
} else {
    go_to_login($_SERVER["REQUEST_URI"]);
}

?>
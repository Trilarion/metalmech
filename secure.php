<?php

/*
    This file is part of MetalMech.
    Copyright (C) 2005  Dzmitry A. Haiduchonak <boom@metalmech.com>

    MetalMech is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    MetalMech is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MetalMech; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


header ("Content-type: image/png");
session_start();
$im = @imagecreate (70, 30)
    or die ("Cannot Initialize new GD image stream");
$background_color = imagecolorallocate ($im, 255, 255, 255);
$text_color = imagecolorallocate ($im, 233, 233, 233);
$text2_color = imagecolorallocate ($im, 230, 230, 230);

$string = randomString(6);

$black1 = imagecolorallocate($im, 0, 0, 0);
$black2 = imagecolorallocate($im, 10, 10, 10);
$black3 = imagecolorallocate($im, 20, 20, 20);
$black4 = imagecolorallocate($im, 30, 30, 30);


imagestring ($im, 4, 1, 0,  "SeCURe", $text_color);
imagestring ($im, 4, 1, 15,  "sECuRE", $text_color2);
imagestring ($im, 4, 20, 5,  "secure", $text_color);

$_SESSION["confirm_word"]=md5(strtolower($string));


$ax=intval(rand(0,2));
$ay=intval(rand(0,3));
$fs=intval(rand(3,5));
imagechar($im, $fs, 5+$ax, 5+$ay, $string[0], $black1);
$ax=intval(rand(0,2));
$ay=intval(rand(0,3));
$fs=intval(rand(3,5));
imagechar($im, $fs, 15+$ax,5+$ay, $string[1], $black2);
$ax=intval(rand(0,2));
$ay=intval(rand(0,3));
$fs=intval(rand(3,5));
imagechar($im, $fs, 25+$ax, 5+$ay, $string[2], $black3);

$ax=intval(rand(0,2));
$ay=intval(rand(0,3));
$fs=intval(rand(3,5));
imagechar($im, $fs, 35+$ax, 5+$ay, $string[3], $black4);

$ax=intval(rand(0,2));
$ay=intval(rand(0,3));
$fs=intval(rand(3,5));
imagechar($im, $fs, 45+$ax, 5+$ay, $string[4], $black4);

$ax=intval(rand(0,2));
$ay=intval(rand(0,3));
$fs=intval(rand(3,5));
imagechar($im, $fs, 55+$ax, 5+$ay, $string[5], $black4);

imageline($im, 0,0,70,0, $black1);
imageline($im, 0,0,0,30, $black1);
imageline($im, 69,0,69,30, $black1);
imageline($im, 0,29,70,29, $black1);


imagepng ($im);
imagedestroy ($im);

function randomString($len) {
//   srand(date("s"));
   $possible="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   $str="";
   while(strlen($str)<$len) {
      $str.=substr($possible,(rand()%(strlen($possible))),1);
   }
   return($str);
}

?>
